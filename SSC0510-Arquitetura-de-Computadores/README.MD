# SSC0510_Arquitetura-de-Computadores

- Professor: Eduardo do Valle Simões
- Email: simoes@@@@@@@icmc.usp.br
- Departamento de Sistemas de Computação – ICMC - USP
- Grupo de Sistemas Embarcados e Evolutivos
- Laboratório de Computação Reconfigurável

## Alunos de 2021 - Segundo semestre PROVA:  https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0510-Arquitetura-de-Computadores/prova.pdf
- Responder a prova em Grupo (o mesmo Grupo da apresentacao do Trabalho) e colocar o PDF da Resposta COMO UM ARQUIVO em uma pasta chamada PROVA no seu GUITHUB / GITLAB (O mesmo link dos slides da APRESENTAÇÃO DO TRABALHO... /PROVA/Resposta_da_Prova.pdf)
- Responder até QUINTA-FEIRA, dia 13 de Janeiro 2022

## Alunos de 2021 - Segundo semestre
- Lista de Presença - Favor assinar a Lista de Presença durante o horario das aulas - https://docs.google.com/spreadsheets/d/1xinFOyZHZA7YXygnpgPlY14TlVbwzIto6mpL_naHPNQ/edit?usp=sharing
- Você pode acompanhar sua Frequência aqui: https://docs.google.com/spreadsheets/d/1BzEpk028gMJ7KfszSbZIAgkRDOkh7qiJ0WsQPgFQI8o/edit?usp=sharing
- As aulas começam dia 17/08/2021 e serão transmitidas pelo google Meet (sempre com o código https://meet.google.com/nwn-osys-cgu) no horário normal da disciplina: Terça 19:00h-20:40h
- As aulas serâo gravadas e disponibilisadas nessa plataforma
- Os alunos serão divididos em grupos de 3-4 alunos para implementação do trabalho prático para a avaliação 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!


## Avaliação
- A avaliação será por meio de uma PROVA e da apresentação de um trabalho - Escolha um tema relativo a Arquitetura de Computadores, como por exemplo uma determinada Arquitetura de processador que você tenha interesse para apresentar.
- Os alunos serão divididos em grupos de 2-3 alunos para apresentação do trabalho prático para a avaliação 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!

### Data da Prova: xx/xx/2021

### Apresentação dos trabalhos Turma 2021 - Segundo semestre:
- Insira as informações do seu grupo e marque o dia escolhido para apresentacao no arquivo do GoogleDocs:
https://docs.google.com/spreadsheets/d/18pFcYwvV9XkGrhZkdOhIoINsWKuKjthUsu5GGkf1xwM/edit?usp=sharing

  - Apresentação dos trabalhos pelo Google Meet será na SEMANA XX de Dezembro

  - Insira seu projeto e reserve um horario para apresentar no arquivo do GoogleDocs, informando: TÍTULO DO PROJETO - nomes dos alunos - NUMERO USP dos alunos - link pro github/gitlab


### Documentação do Trabalho no Github/Gitlab
- Criar uma conta sua no Github/gitlab
- Os projetos devem conter um Readme explicando a Arquitetura escolhida
- Incluir no seu Github/gitlab: slides da apresentação e um um VÍDEO DE VOCË explicando a arquitetura (pode ser somente uma captura de tela...) - Upa o vídeo no youtube ou no drive e poe o link no Readme.
- Além do VÍDEO DE VOCË explicando o projeto, TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE por google Meet: a ser informado o link




# Aulas a Distância 2021 - Segundo Semestre

## Aula 01
- Link para a Aula (17/08/21) - https://drive.google.com/file/d/1oZMZynLN9cYUdqbVOQ1SLcqTQ0JPeBcJ/view?usp=sharing
- Material (Fotos da Lousa, PDFs...) - https://gitlab.com/simoesusp/disciplinas/-/tree/master/SSC0510-Arquitetura-de-Computadores/MaterialAulaDistancia


## Aula 02
- Link para a Aula (24/08/21) - https://drive.google.com/file/d/1XmGNyePtNYML0-v1k0BEvT_2kD7hpX6l/view?usp=sharing

## Aula 03
- Link para a Aula (31/08/21) - https://drive.google.com/file/d/1o2RjeJkLE1q9Lzip1pvgIu-BMN7yYrwi/view?usp=sharing

## Aula 04
- Link para a Aula (14/09/21) - https://drive.google.com/file/d/1nrx_jlG4FaN4aBcnq4c3RUkl8O8RDzhX/view?usp=sharing

## Aula 05
- Link para a Aula (21/09/21) - https://drive.google.com/file/d/18hIcoxMXcFzM4aoXjK9djtOtVaVpUP2l/view?usp=sharing

## Aula 06
- Link para a Aula (05/10/21) - https://drive.google.com/file/d/1i9PYNgT204F8JGuAnnC6VISzoejWMDgY/view?usp=sharing

## Aula 07
- Link para a Aula (19/10/21) - https://drive.google.com/file/d/1TygsCwHzK9RtSmyj5B0KeaVq4OKc1Ve9/view?usp=sharing

## Aula 08
- Link para a Aula (26/10/21) - https://drive.google.com/file/d/133XbGt6nv0aMEImHNBIqEzf2ZxSasyZ0/view?usp=sharing

## Aula 09
- Link para a Aula (09/11/21) - https://drive.google.com/file/d/1kIJjv0Rz4ZbEZkPZjTh_OeA7m_yJM1Pd/view?usp=sharing

## Aula 10
- Link para a Aula (16/11/21) - https://drive.google.com/file/d/15R2QGJVzYizgZrDy3KiwQf_oVcbRDosm/view?usp=sharing

## Aula 11
- Link para a Aula (23/11/21) - https://drive.google.com/file/d/1B5wsfqUGPDE1ds_d1KqdShu11KmDsXtn/view?usp=sharing

## Aula 12
- Link para a Aula (30/11/21) - https://drive.google.com/file/d/1R04-RPH0YubRIW4UBTNpaYA90V0foK45/view?usp=sharing

## Aula 13
- Link para a Aula (07/12/21) - https://drive.google.com/file/d/1DJ8MgGs32AZCI-Tr-0oU4o-vpZQVoB7G/view?usp=sharing

## Aula 14
- Link para a Aula (21/12/21) - https://drive.google.com/file/d/1Sud5KJVhWEx0R_T6MkW3zTu3nyu5_bGs/view?usp=sharing

## Aula 15
- Link para a Aula (21/12/21) - 
- https://docs.google.com/document/d/1plCJe9hSlcHgxKpfq-1McsUPj81peC-xAtQdue-QWBA/edit?usp=sharing














# Informações da Disciplina SSC0510_Arquitetura-de-Computadores
Objetivos
Introduzir os conceitos da Arquitetura de von Neuman e os aprimoramentos que esta arquitetura vem experimentando.

Professor
Eduardo Simoes

email: simoes AT icmc.usp.br

Programa
Arquitetura de von Neuman. Técnicas de Pipeline. Introdução às Arquiteturas RISC e CISC. Processadores Superescalares. Processadores vetoriais. Arquiteturas paralelas. Análise de Arquiteturas Comerciais.

Avaliação
 	Método
 	1 Prova (peso 7) e 1 Trabalho (em dupla) com apresentação para a turma (peso 3)
  
  Data da Prova: 12/11/2019

 	Critério
 	Média ponderada das notas em provas, trabalhos e seminários.

 	Norma de Recuperação
 	(NP-2) / 5 * Mrec + 7 - NP, se Mrec > 5 Max { NP, Mrec }, se Mrec < 5
Bibliografia
"	Livro Texto:

PATTERSON, D.A.; HENNESSY, J.L. Computer Organization and Design: The Hardware/Software Interface, Morgan Kaufmann, 1994.
STALLINGS, W. Arquitetura e Organização de Computadores, Prentice Hall, 5a. ed., 2002.
