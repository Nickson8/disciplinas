# PRECARIZAÇÃO DO TRABALHO

## A) PROBLEMA:
### 0) Direitos do Cidadao --> Possibilitar assinar um coontrato de trabalho --> Vender sua mao de obra
- Precisa do Estado
  - Garantir os Direitos
  - Garantir os Contratos
  - Garantir a Moeda
### 1) Neoliberalismo -> Estado Minimo
### 2) Microempreendedor individual (MEI) -  é uma figura jurídica do Brasil, é a pessoa que trabalha por conta própria ou um microempreendedor individual,
### 3) UBERIZACAO (MEI + Aplicativo)
### 4) Perigo Atual: Privatizacao do Salario INDRETO: - SUS + Educacao + Lazer 





## B) CAPITALISMO REFORMISTA:

### 1) Lula ONU (19/9)
- criticou as empresas de aplicativo por promoverem a precarização do trabalho com a chamada “uberização”. 
- “abolir os direitos trabalhistas duramente conquistados”

### 2) LULA + BIDEN (20/9) -> “Parceria pelos Direitos dos Trabalhadores e Trabalhadoras” - fortalecer sindicatos e direitos trabalhistas nos dois países:
- Proteger os direitos dos trabalhadores, tal como descritos nas convenções fundamentais da OIT [Organização Internacional do Trabalho], capacitando os trabalhadores, acabando com exploração de trabalhadores, incluindo trabalho forçado e trabalho infantil;
- Promoção do trabalho seguro, saudável e decente, e responsabilização no investimento público e privado;
- Promover abordagens centradas nos trabalhadores para as transições digitais e de energia limpa;
- Aproveitar a tecnologia para o benefício de todos; e
- Combater a discriminação no local de trabalho, especialmente para mulheres, pessoas LGBTQI e grupos raciais e étnicos marginalizados.

